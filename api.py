import requests
import pandas as pd
import time
import yfinance as yf
import os
from datetime import date
import shutil
import os
import json
from datetime import datetime
import lxml
from lxml import html
import numpy as np


stocks = ['VALE3.SA', 'ITUB4.SA', 'BBDC4.SA', 'B3SA3.SA', 'PETR4.SA', 'ABEV3.SA', 'ITSA4.SA', 'PETR3.SA', 'BBAS3.SA', 'JBSS3.SA']
today = str(date.today())


# Get History Prices 
def get_history(stock):
	df = yf.Ticker(stock).history()
	#del df['Stock Splits']
	#del df['Dividends']
	#del df['Volume']
	#df['Date'] = df.index

	df.reset_index(drop=False, inplace=True)

	df['Date'] = df['Date'].map(lambda x: str(x)[:10])

	return df.to_json()
	#return yf.Ticker(stock).history().to_json()


# Get Key stats
def get_key_stats(stock):
	stock_url = 'https://sg.finance.yahoo.com/quote/{0}/key-statistics?p={0}'.format(stock)
	df_list = pd.read_html(stock_url)
	result_df = df_list[0]
 
	for df in df_list[1:]:
		result_df = result_df.append(df)
 
	return result_df.set_index(0).T.to_json()


# Get Current Price
def get_current_price(stock):
	stock_url = 'https://sg.finance.yahoo.com/quote/{0}/summary?p={0}'.format(stock)

	headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
		'Accept-Encoding': 'gzip, deflate, br',
		'Accept-Language': 'en-US,en;q=0.9',
		'Cache-Control': 'max-age=0',
		'Pragma': 'no-cache',
		'Referrer': 'https://google.com',
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
	}

	page = requests.get(stock_url, headers)
	tree = html.fromstring(page.content)
	spans = tree.xpath("//span/text()")

	# Checking span before and after the price to make sure we're getting the right one.
	# Return price
	for i in range(len(spans)):
		if spans[i][:3] == 'Sao' and spans[i+2].count('%')==1:
			price = spans[i+1]

	return price