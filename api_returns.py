import requests
import pandas as pd
import sqlalchemy as sql
import altair as alt
import json, csv
import os
import api

alt.data_transformers.disable_max_rows()
current_dir = os.getcwd()

def get_data(stocks, metric):
    if metric == 'keyratios':
        c = api.get_key_stats(stocks)
        c = json.loads(c)

    if metric == 'price':
        c = api.get_history(stocks)
        c = json.loads(c)
    
    if metric == 'summary':
        print('x')

    return c


def get_financials(stocks):
    c = get_data(stocks, 'financials')['financials']
    dfs = {}

    for group in c.keys():

        if group != 'financial_template_parameters':
            dfs[group] = {}

            for k in c[group]:

                if k != 'Fiscal Year':
                    dfs[group][k] = pd.DataFrame(c[group][k], index=c[group]['Fiscal Year'])

    return dfs


# SAVING TO EXCEL
def financials_to_excel(content):

    for fn in content:

        with pd.ExcelWriter('data//' + fn + '.xlsx', engine='xlsxwriter') as writer:
            for k in content[fn]:
                content[fn][k].to_excel(writer, sheet_name=k)


### keyratios
def get_selected_ratios(stocks):
    c =  get_data(stocks, 'keyratios')

    x = {}
    for key in c:
        for k, v in c[key].items():
            x[key] = v

    c = x

    # PLACEHOLDER > FIX KEY METRICS
    return {'EPS': c['Diluted EPS (ttm)'],
            'ROIC %': c['Return on assets (ttm)'],
            'ROE %': c['Return on equity (ttm)'],
            'PB': c['Price/book (mrq)'],
            'Forward PE': c['Forward P/E 1'],
            'PE': c['Price/sales (ttm)'],
            'Avg Vol(10 days)': c['Avg vol (10-day) 3']}

# Create dictionary with summaries?
## summary
def get_summary_data(stocks):
    #s = get_data(stocks, 'summary')

    return {'Company': stocks,
            'Ticker': stocks,
            'Sector': stocks,
            'Description': stocks}


## price
def get_price_data(stocks):

    p = pd.DataFrame(get_data(stocks, 'price'),
        columns=['Date', 'Open'])
    p.Date = p.Date.map(pd.to_datetime)
    p.set_index('Date', inplace=True, drop=True)

    avg_ltm = p.last('12M').Open.mean()
    avg_l30d = p.last('30D').Open.mean()
    vol_ltm = p.last('12M').Open.std()
    vol_l30d = p.last('30D').Open.std()

    return {'Daily Prices': p,
            'LTM Avg. Price': avg_ltm,
            'LTM Volatility': vol_ltm,
            'L30D Avg. Price': avg_l30d,
            'L30D Volatility': vol_l30d}

def get_dashboard_data(stocks):
    p = get_price_data(stocks)
    s = get_summary_data(stocks)
    d = get_selected_ratios(stocks)

    # merge everything on a single dictionry
    p.update(s)
    p.update(d)

    return p

def get_chart_data(stock, content):

	dt = content['Daily Prices']

	dt = dt.last('5Y')
	dt['Year'] = dt.index.year
	dt['Date'] = dt.index

	dt_l5y = dt.last('5Y')
	dt_l5y['Period'] = '5A'

	dt_ltm = dt.last('12M')
	dt_ltm['Period'] = '12M'

	dt_cy = dt[dt.index.year==dt.index.year.max()]
	dt_cy['Period'] = str(dt.index.year.max())

	dt_l6m = dt.last('6M')
	dt_l6m['Period'] = '6M'

	dt_l3m = dt.last('3M')
	dt_l3m['Period'] = '3M'

	dt_l1m = dt.last('1M')
	dt_l1m['Period'] = '1M'

	dt = pd.concat([dt_l5y, dt_ltm, dt_cy, dt_l6m, dt_l3m, dt_l1m])

	return dt


def get_chart(dt):
    avg_dt = dt.groupby('Year').mean().reset_index()

    selection = alt.selection(type='single', fields=['Period'], init={'Period': '2020'})

    line_chart = alt.Chart(dt, width=633).mark_line().encode(
            x=alt.X('Date', title=None),
            y=alt.Y('Open', scale=alt.Scale(nice=True, zero=False), title=None),
            tooltip=[alt.Tooltip('Open', title='Preço'),
                    alt.Tooltip('Date', title='Data')]
    ).transform_filter(
        selection
    )

    quick_select = alt.Chart(dt, width=633).mark_text().encode(
        x=alt.X('Period:N', axis=None, title=None, sort=['5A', '12M', '6M', '3M', '1M', '2020']),
        text='Period',
        color=alt.condition(selection,
            alt.ColorValue("steelblue"), 
            alt.ColorValue("grey"))
    ).add_selection(
        selection
    )

    chart = alt.vconcat(quick_select, line_chart).configure_concat(
        spacing=5
    ).add_selection(alt.selection_single())

    return chart