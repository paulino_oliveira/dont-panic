import altair as alt
import api_returns as gf
import json
from waitress import serve
from api import get_current_price

from flask import Flask, render_template

#alt.renderers.enable('altair_viewer')

# FLASK SERVER
app = Flask(__name__)

message = 'Por quê mesmo?'
stocks = ['VALE3.SA', 'ITUB4.SA', 'BBDC4.SA', 'B3SA3.SA', 'PETR4.SA', 'ABEV3.SA', 'ITSA4.SA', 'PETR3.SA', 'BBAS3.SA', 'JBSS3.SA']


# PETR4 on homepage
@app.route('/')
def home():
    return index('PETR4.SA')

@app.route('/api/chart', methods=['GET'])
def home1():
    return api('PETR4.SA')


@app.route('/<stock>')
@app.route('/index/<stock>')
def index(stock):
    c = gf.get_dashboard_data(stock)
    key_metrics = [
        {'name': 'ROI',
         'description': 'Retorno sobre o investimento',
         'value': c['ROE %']},
         {'name': 'P/L',
          'description': 'Preço da ação dividido pelo lucro',
          'value': c['PE']},
         {'name': 'PBV',
          'description': 'Preço dividido pelo patrimônio',
          'value': c['PB']},
          {'name': 'Preço & Volatilidade',
          'description': 'Preço: 30D LTM      Volat: 30D LTM',
          'value': ''}
    ]
    
    # Values to be shown on Preco & Volatilidade Modal
    preco_30 = "Ultimos 30 dias: R${0:.2f}".format(c['L30D Avg. Price'])
    preco_ltm = "Ultimos 12 meses: R${0:.2f}".format(c['LTM Avg. Price'])
    volat_30 = "Ultimos 30 dias: R${0:.2f}".format(c['L30D Volatility'])
    volat_ltm = "Ultimos 12 meses: R${0:.2f}".format(c['LTM Volatility'])
    # PLACEHOLDER #
    # Time to buy function 
    if c['L30D Avg. Price'] > c['LTM Avg. Price']:
        buy_time = 'Bom para compra!'
    else:
        buy_time = 'Não é hora de comprar!'


    return render_template('index.html',
        message=message,
        key_metrics=key_metrics,
        stocks=stocks,
        company=c['Company'],
        ticker=stock,
        price='R$ {}'.format(get_current_price(stock)),
        #price='R$ {:.2f}'.format(c['Daily Prices'].Open[-1]),
        buy_time = buy_time,
        preco_30 = preco_30,
        preco_ltm = preco_ltm,
        volat_30 = volat_30,
        volat_ltm = volat_ltm
    )

@app.route('/<stock>/api/chart', methods=['GET'])
def api(stock):
    c = gf.get_dashboard_data(stock)
    dt =  gf.get_chart_data(stock, c)
    chart = gf.get_chart(dt)
    j = json.loads(chart.to_json())

    return j

    

#if __name__ == '__main__':
    #app.run(debug=True) #Development
serve(app, port=80) #Production


